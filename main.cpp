
#include "widget.h"

#include <QApplication>
#include <QStyleFactory>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));

    // 设置全局字体
    QFont globalFont("微软雅黑", 12);

    Widget w;
    // 设置字体
    w.setFont(globalFont);

    // 在主窗口构造函数中加载 QSS 文件
    QFile qss(":/style.qss");
    if(qss.open(QFile::ReadOnly)) {
        QString styleSheet = QLatin1String(qss.readAll());
        // qDebug("[%s]", qPrintable(styleSheet));
        a.setStyleSheet(styleSheet);
        qss.close();
    }

    w.show();
    return a.exec();
}
